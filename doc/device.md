# 设备接口部分

- 设备主要概念
- Characteristic: 属性
- Service: 服务
- Accessory: 配件
- Container: 容器

## 设备登录
- 路径: login
```
{
  "path": "login",
  "method": "post",
  "data": {
    "uuid": "8bd53509433b457b",
    "key": "i9U1CUM7RXuxUVHvSvHP1Q==",
    "date": "2018/11/30 19:10:30.47"
  }
}
```
- 登录返回结果
```
{
  "path": "login",
  "method": "post",
  "state": "ok",
  "data": {
    "uuid": "8bd53509433b457b",
    "date": "2018-11-30 19:10:31"
  }
}
```

## 设备绑定信息推送
- 路径: bind
- 设备的版本信息，以及设备的家庭信息上传至服务器
```
{
  "path": "bind",
  "method": "put",
  "data": {
    "firmware": "0.1.1",
    "home": "64e649c2-897c-4409-a778-d3d2611e7e24",
    "date": "2018/11/30 19:19:09"
  }
}
```

## 设备容器信息
- 容器描述获取
- 路径: container
```
{
    "path":"container/info",
    "method":"get"
}
```
- 返回结果
```
{
  "path": "container/info",
  "method": "get",
  "state": "ok",
  "data": {
    "date": "2018/12/01 03:19:09",
    "firmware": "0.0.1",
    "version": 3
  }
}
```

- 容器获取
- 路径: container
- 容器的详细信息，至一个设备所有所有的服务信息，包括子设备
```
{
  "path": "container",
  "method": "get"
}
```
- 返回结果
```
{
  "path": "container",
  "method": "get",
  "state": "ok",
  "data": {
    "id": "a4c2532b877846e8",
    "version": 2,
    "accessories": [
      {
        "aid": "",
        "type": 2,
        "services": [
          {
            "sid": 20,
            "type": "20",
            "characteristics": [
              {"cid": 1,"type": "24","value": "00158d00027567c9","format": "string"},
              {"cid": 2,"type": "23","value": "0000","format": "string"}
              ....
            ]
          }
        ]
      },
      .....
    ]
  }
}
```

## 容器属性

- 获取所有的属性状态
- 路径: characteristic
```
{
    "path":"characteristics",
    "method":"get"
}
```
- 返回结果
```
{
  "path": "characteristics",
  "method": "get",
  "state": "ok",
  "data": [
    {"aid": "00158d0001d26ebb","sid": 20,"cid": 1,"value": "00158d00027567c9"},
    {"aid": "00158d0001d26ebb","sid": 20,"cid": 2,"value": "0000"}
    ......
  ]
}
```

- 容器属性控制
- 路径: characteristics
```
{
  "path": "characteristics",
  "method": "post",
  "data": [
    {
      "aid": "00158d0001d26ebb",
      "sid": 3,
      "cid": 1,
      "value": 1
    }
  ]
}
```

- 属性状态更新推送
- 属性控制之后，状态更新，或传感器数据上报，手动控制状态更新的上报
- 路径: characteristics
```
{
  "path": "characteristics",
  "method": "put",
  "data": [
    {
      "aid": "00158d0001d26ebb",
      "sid": 3,
      "cid": 1,
      "value": "03:16"
    }
  ]
}
```


