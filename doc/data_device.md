# 设备调试数据

## Philips Hue
- 设备ID: 00178801024490f7
```
{
    "aid": "00178801024490f7",
    "sid": 11,
    "cid": 1,
    "value": 1
}
```

## IKEA Light
- 设备ID: d0cf5efffe11f992
```
{
    "aid": "d0cf5efffe11f992",
    "sid": 1,
    "cid": 1,
    "value": 0
}
```

## Philips Hue Go
- 设备ID: 001788010116b471
```
{
    "aid": "001788010116b471",
    "sid": 11,
    "cid": 1,
    "value": 0
}
```

## Philips Hue LightStrip
- 设备ID: 0017880103a0f87e
```
{
    "aid": "0017880103a0f87e",
    "sid": 11,
    "cid": 1,
    "value": 0
}
```



