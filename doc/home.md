## 家庭接口部分

- 家庭的主要组成部分
- 家庭基本信息 (id、name、version、position)
- Members: 家庭成员
- Containers: 设备容器列表
- Rooms: 房间列表
- Service: 服务列表
- Scenes: 家庭场景列表
- Automations: 自动化列表

## 获取家庭基本信息
- 路径: home/info
- 当客户端登录后的第一步，获取家庭基本信息，来确认是否需要更新家庭数据库
```
{
    "path": "home/info",
    "home": "64e649c2-897c-4409-a778-d3d2611e7e24",
    "method": "get"
}
```
- 返回结果
```
{
    "path": "home/info",
    "home": "64e649c2-897c-4409-a778-d3d2611e7e24",
    "method": "get",
    "state": "ok",
    "data": {
        "id": "64e649c2-897c-4409-a778-d3d2611e7e24",
        "version": 4,
        "name": "云の家",
        "members": [
            {
                "id": "34c92c64-6d84-490f-8e25-e27e1c8bf58e",
                "role": "admin"
            }
        ]
    }
}
```

- 家庭详细信息
- 路径: home
- 当本地版本大于服务器版本时，向服务器post家庭信息
- 当本地版本小于服务器版本时，向服务器get家庭信息
```
{
    "path": "home",
    "home": "64e649c2-897c-4409-a778-d3d2611e7e24",
    "method": "get",
    "state":"ok",
    "data": {
        "name": "云の家",
        "id": "64e649c2-897c-4409-a778-d3d2611e7e24",
        "version": 6,
        "members": [ ... ],
        "containers": [ ... ],
        "rooms": [ ... ],
        "services": [ ... ],
        "scenes": [ ... ],
        "automations": [ ... ]
    }
}
```

- 容器属性的状态
- path: home/container/characteristic
- 当确认家庭的数据库本地版本和服务器版本一只时，获取容器属性的状态
```
{
    "path": "home/container/characteristic",
    "home": "64e649c2-897c-4409-a778-d3d2611e7e24",
    "method": "get"
}
```
- 返回结果
- 先检查本地的容器版本与最新容器版本，确定是否需要更新容器数据
```
{
    "path": "home/container/characteristic",
    "home": "64e649c2-897c-4409-a778-d3d2611e7e24",
    "method": "get",
    "state": "ok",
    "data": [
        {
            "container": "8bd53509433b457b",
            "version": 7,
            "reachable": true,
            "characteristics": [
                {"aid":"8bd53509433b457b","sid":20,"cid":1,"value":"00158d000113c3a9"}
                ......
            ]
        }
    ]
}
```

- 容器信息
- 路径: home/container
- 当服务器的容器版本大于本地的容器版本时，想服务器请求容器信息
```
{
    "path": "home/container",
    "home": "64e649c2-897c-4409-a778-d3d2611e7e24",
    "method": "get"
}
```
- 返回结果
```
{
    "path": "home/container",
    "home": "64e649c2-897c-4409-a778-d3d2611e7e24",
    "method": "get",
    "state": "ok",
    "data": [
        {
            "id": "8bd53509433b457b",
            "home": "64e649c2-897c-4409-a778-d3d2611e7e24",
            "reachable": true,
            "version": 7,
            "accessories": [ ... ]
        }
    ]
}
```

- 更新设备属性
- 路径: home/characteristic
```
{
    "path": "home/characteristic",
    "method": "post",
    "home": "64e649c2-897c-4409-a778-d3d2611e7e24",
    "data": [
        {"aid": "00178801024490f7","sid": 11, "cid": 1, "value": 1},
        {"aid": "0017880103a0f87e","sid": 11,"cid": 1,"value": 0},
        ......
    ]
}
```

- 获取属性历史记录
- 路径: home/characteristic/log
- 请求数据格式
```
{
    "path": "home/characteristic/log"
    "method": "get",
    "home": "64e649c2-897c-4409-a778-d3d2611e7e24"
    "data": {
        "aid": "00178801024490f7",
        "sid": 11,
        "cid": 1,
        "before": "2006-01-02 15:04:05",
        "after": "2006-01-01 15:04:05"
    }
}

```
- 返回数据格式
```
{
    "path": "home/characteristic/log"
    "home": "64e649c2-897c-4409-a778-d3d2611e7e24"
    "state": "ok"
    "data": {
        "aid": "00178801024490f7",
        "sid": 11,
        "cid": 1,
        "before": "2006-01-02 15:04:05",
        "after": "2006-01-01 15:04:05"
        "logs": [
            {"time": "2006-01-01 15:04:05", "value": 27.5},
            ......
        ]
    }
}
```



- 设备容器属性有更新
- 路径: home/container/characteristic
```
{
    "path": "home/container/characteristic",
    "method": "put",
    "home": "64e649c2-897c-4409-a778-d3d2611e7e24",
    "data": {
        "container": "8bd53509433b457b",
        "reachable": true,
        "characteristics": [
            {"aid": "8bd53509433b457b","sid": 3,"cid": 1,"value": "18:43"}
        ]
    }
}
```

- 获取家庭成员信息
- 路径: home/member
```
{
    "path": "home/member",
    "method": "get",
    "state": "ok",
    "data": [
        {
            "id": "ce173611-1d3d-4414-94ff-1764fc7812f3",
            "role": "admin",
            "name": "默认用户名",
            "account": "785492565@qq.com",
            "icon": "jarvis",
            "state": "leave"
        }
    ]
}
```

- 更新家庭成员状态信息
- 路径: home/member/location
```
{
    "path": "home/member/location",
    "method": "post",
    "home": "187c7894-1d3d-442b-9d3a-d2ebcfa86a6c",
    "data": {
        "member": "ce173611-1d3d-4414-94ff-1764fc7812f3",
        "state": "leave"
    }
}
```

- 执行场景
- 路径: home/scene
```
{
    "path": "home/scene",
    "method": "post",
    "home": "187c7894-1d3d-442b-9d3a-d2ebcfa86a6c",
    "data": {
        "scene": "43ddede5-7a9f-4b7d-878f-d4be3e349b61",
        "type": "execute"
    }
}
```

