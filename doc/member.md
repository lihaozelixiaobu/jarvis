# 用户接口部分

## 用户客户端与服务器通信消息接口
- path: 资源路径 (login、home .....)
- method: 资源操作方法(post、get、put)
- home?: 家庭uuid，当消息需要转发至家庭时需要此字段
- data: 消息内容
- state?: 请求结果状态

## 用户账号登录
- 路径: member/login
- 用户登录的第一种方式 (account+password)
- account: 可以用用户手机号、也可以是邮箱
- password: 用户密码
```
{
    "path": "member/login",
    "method": "post",
    "data": {
        "account": "18565381403",
        "password": "123456",
        "dev_t": "macbook"
    }
}
```

- 账号密码登录成功后返回结果
- 用户登录之后返回用户的基本信息，此用户拥有的家庭列表(homes)
- 用户的唯一id，消息认证session、用户的头像icon、用户名name
```
{
    "path": "member/login",
    "state": "ok",
    "data": {
        "homes": [
            {
                "id": "64e649c2-897c-4409-a778-d3d2611e7e24",
                "role": "guest"
            }
        ],
        "id": "34c92c64-6d84-490f-8e25-e27e1c8bf58e",
        "icon": "daodao",
        "session": "79484cccce66801924f4033938b76f29",
        "name": "刀刀",
        "date": "2018-11-30 16:07:58"
    }
}
```

- 当用户第一个登录成功后，后面便可以使用id+session方式进行通信
- 客户端使用http向服务器post或get消息时，智能通过id+session的方式进行
- 同一个账号可以不同的客户端可以同时在线，如 phone/pad/web/pc
- 不同的客户端可以同时接受推送消息
```
{
    "path": "member/login",
    "method": "post",
    "data": {
        "id": "34c92c64-6d84-490f-8e25-e27e1c8bf58e",
        "session": "2aa119d77f1fab399884ebe198adfa5d",
        "dev_t": "pc"
    }
}
```

## 用户账号登出
- 路径: member/logout
```
{
    "path": "member/logout",
    "method": "post",
    "data": {
        "id": "34c92c64-6d84-490f-8e25-e27e1c8bf58e",
        "session": "2aa119d77f1fab399884ebe198adfa5d",
        "dev_t": "pc"
    }
}
```
- 用户账号登出返回结果
```
{
    "path": "member/logout",
    "state": "ok",
    "data": {
        "id": "34c92c64-6d84-490f-8e25-e27e1c8bf58e",
        "date": "2018-11-30 16:07:58",
        "dev_t": "pc"
    }
}
```


## 更新APNS推送信息
- 路径: member/push/apns
```
{
    "path":"member/push/apns",
    "method":"post",
    "data": {
        "topic":"adai.design.home",
        "apns": "f0c054609c6501525f0d17db21abfdab1fa93a7a38dad8aa96db7f31f5afccec",
        "dev_t":"phone"
    }
}
```




## 修改用户名称或头像
## 新建家庭、删除家庭
## 请求加入家庭、邀请用户加入家庭
## 同意加入家庭













