package member

import (
	"adai.design/jarvis/common/db"
	"adai.design/jarvis/common/log"
	"encoding/json"
	"errors"
	"github.com/globalsign/mgo/bson"
	"time"
)

//
// 用户可以登录的客户端类型
// 同一个账号可以在不通类型的客户端上同时在线
// 同一中类型的客户端只允许一个设备在线
// 	1、 用于同时拥有iphone和android手机，同一时刻，只允许一台手机在线
//  2、 手机(iphone)和平板电脑(ipad)是可以同时登录在线，并且同时收到
//
const (
	ClientTypePhone = "phone"
	ClientTypePad   = "pad"
	ClientTypePC    = "pc"
	ClientTypeWeb   = "web"
)

// 注册用户登录的客户端描述信息
type Client struct {
	DevType string `json:"dev_t" bson:"dev_t"`
	Session string `json:"session" bson:"session"`
	// 设备型号,如: iphone6s
	ModelType string `json:"model_t,omitempty" bson:"model_t,omitempty"`
	// 苹果推送Token
	APNS  string `json:"apns,omitempty" bson:"apns,omitempty"`
	Badge int    `json:"badge,omitempty" bson:"badge,omitempty"`
	// 最后一次登录时间，用来确定Token是否失效
	Last    time.Time `json:"-" bson:"last,omitempty"`
	LastStr string    `json:"last,omitempty" bson:"-"`
}

func (c *Client) String() string {
	c.LastStr = c.Last.Format("2006-01-02 15:04:05")
	buf, _ := json.Marshal(c)
	return string(buf)
}

type Clients struct {
	UserId  string    `json:"uid" bson:"uid"`
	Clients []*Client `json:"clients" bson:"clients"`
}

func (cs *Clients) String() string {
	buf, _ := json.Marshal(cs)
	return string(buf)
}

// 将客户端信息保存至数据库
func (cs *Clients) Save() error {
	session, err := db.GetSession()
	if err != nil {
		return err
	}
	defer session.Close()

	log.Trace("%s", cs)

	collection := session.DB("member").C("clients")
	_, err = collection.Upsert(bson.M{"uid": cs.UserId}, cs)
	return err
}

// 移除制定的客户端
func (cs *Clients) RemoveClient(session, devType string) error {
	for i, client := range cs.Clients {
		if client.Session == session && client.DevType == devType {
			cs.Clients = append(cs.Clients[:i], cs.Clients[i+1:]...)
		}
	}
	cs.Save()
	return nil
}

// 设备端登录
type loginRequest struct {
	// account+password登录
	Account  string `json:"account,omitempty"`
	Password string `json:"password,omitempty"`
	// id+session登录
	Id    string `json:"id,omitempty"`
	Token string `json:"session,omitempty"`
	// 设备类型 phone/pad/pc/web
	DevType string `json:"dev_t"`
	Date    string `json:"data,omitempty"`
}

// 登录返回结果
type loginResponse struct {
	Id    string `json:"id"`
	Token string `json:"token"`
	// 用户基本信息
	Date string  `json:"data,omitempty"`
	Name string  `json:"name,omitempty"`
	Icon string  `json:"icon,omitempty"`
	Home []*Home `json:"homes,omitempty"`
}

func login(data []byte) (a *Account, c *Client, err error) {
	var request loginRequest
	err = json.Unmarshal(data, &request)
	if err != nil {
		err = errors.New("failed")
		return
	}

	if len(request.Account) != 0 {
		a, c, err = accounts.Login(request.Account, request.Password, request.DevType)
		return
	} else if len(request.Id) != 0 {
		a, c, err = accounts.SessionCheck(request.Id, request.Token, request.DevType)
		return
	} else {
		err = errors.New("failed")
		return
	}
}
