package db

import (
	"github.com/globalsign/mgo/bson"
	"github.com/satori/go.uuid"
)

func NewId() string {
	id, err := uuid.NewV4()
	if err != nil {
		return ""
	}
	return id.String()
}

func NewDeviceId() string {
	id := bson.NewObjectId()
	return id.Hex()
}
