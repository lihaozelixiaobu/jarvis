package db

import (
	"github.com/globalsign/mgo/bson"
	"github.com/satori/go.uuid"
	"testing"
)

func TestClient(t *testing.T) {
	session, err := GetSession()
	if err != nil {
		t.Fatal(err)
	}
	defer session.Close()
}

func TestDBCollection(t *testing.T) {
	session, err := GetSession()
	if err != nil {
		t.Fatal(err)
	}
	defer session.Close()

	collection := session.DB("member").C("account")
	err = collection.Insert(bson.M{"id": "123456"})
	if err != nil {
		t.Fatal(err)
	}
}

func TestUUID(t *testing.T) {
	id, err := uuid.NewV4()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf(id.String())

	oid := bson.NewObjectId()
	t.Logf("%s", oid.Hex())

}
