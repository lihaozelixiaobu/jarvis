package device

import (
	"adai.design/jarvis/server"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/astaxie/beego/httplib"
	"github.com/gorilla/websocket"
	"io/ioutil"
	"testing"
	"time"
)

const testURL = "ws://127.0.0.1:6666/device"

func TestConnect(t *testing.T) {
	c, _, err := websocket.DefaultDialer.Dial(testURL, nil)
	if err != nil {
		t.Fatal(err)
	}
	defer c.Close()
}

type testClient struct {
	reg  *Register
	conn *websocket.Conn
}

func (c *testClient) connect(url string) error {
	websocket.DefaultDialer.HandshakeTimeout = time.Second * 2
	conn, _, err := websocket.DefaultDialer.Dial(testURL, nil)
	if err != nil {
		return err
	}
	if conn == nil {
		return errors.New("connect failed")
	}
	c.conn = conn
	return nil
}

func (c *testClient) disconnect() {
	c.conn.Close()
}

func (c *testClient) writeMsg(msg *Message) error {
	buf, err := json.Marshal(msg)
	if err != nil {
		return err
	}
	return c.conn.WriteMessage(websocket.BinaryMessage, buf)
}

func (c *testClient) readMsg() (*Message, error) {
	msgT, buf, err := c.conn.ReadMessage()
	if err != nil {
		return nil, err
	}
	if msgT != websocket.BinaryMessage {
		return nil, errors.New("invalid-message-type")
	}
	var msg Message
	err = json.Unmarshal(buf, &msg)
	if err != nil {
		return nil, err
	}
	return &msg, nil
}

func (c *testClient) login() error {
	info := &loginInfo{
		ID:   c.reg.Id,
		Key:  c.reg.Secret,
		Date: time.Now().Format("2006-01-02 15:04:05"),
	}
	msg := &Message{
		Path:   msgPathLogin,
		Method: MsgMethodPost,
	}
	msg.Data, _ = json.Marshal(info)
	err := c.writeMsg(msg)
	if err != nil {
		return err
	}

	// 读取登录结果
	ack, err := c.readMsg()
	if err != nil {
		return err
	}
	//log.Info("login ack <- %s", ack)
	if ack.Path != msgPathLogin || ack.Method != MsgMethodPost {
		return errors.New("login failed")
	}

	if ack.State != "ok" {
		return fmt.Errorf("login failed: %s", ack.State)
	}
	return nil
}

func TestClientLogin(t *testing.T) {
	uuid := "be6c87ed-3fc2-466b-b8e1-f504685e5220"
	reg, err := registers.findRegisterById(uuid)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(reg)

	client := testClient{
		reg: reg,
	}
	err = client.connect(testURL)
	if err != nil {
		t.Fatal(err)
	}
	defer client.disconnect()

	err = client.login()
	if err != nil {
		t.Fatal(err)
	}

	//client.conn.SetPongHandler(func(appData string) error {
	//	println("pong")
	//	return nil
	//})
	//
	//go func() {
	//	_, _, err := client.conn.ReadMessage()
	//	if err != nil {
	//		t.Logf("err: %s", err)
	//		return
	//	}
	//}()
	//
	//for i:=0; i<30; i++ {
	//	client.conn.WriteControl(websocket.PingMessage, nil, time.Now().Add(time.Second))
	//	time.Sleep(time.Second)
	//}
}

const statusInfoUrl = "http://127.0.0.1:6666/status"

func getStatus() (*server.StatusInfo, error) {
	request := httplib.Get(statusInfoUrl)
	res, err := request.DoRequest()
	if err != nil {
		return nil, err
	}

	data, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		return nil, err
	}

	var status server.StatusInfo
	err = json.Unmarshal(data, &status)
	if err != nil {
		return nil, err
	}
	return &status, nil
}

// 测试 5K台设备同时登陆
func TestClientsLogin(t *testing.T) {
	rs, err := registers.findRegisterAll()
	if err != nil {
		t.Fatal(err)
	}

	status, err := getStatus()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("before: %s", status)

	var testCount = 5000
	if len(rs) < testCount {
		testCount = len(rs)
	}
	conns := make(chan int, testCount)

	var clients = make([]testClient, testCount)
	for i := 0; i < testCount; i++ {
		go func(i int) {
			time.Sleep(time.Duration(i/2) * time.Millisecond)
			clients[i].reg = rs[i]
			err := clients[i].connect(testURL)
			if err != nil {
				conns <- i
			}
			err = clients[i].login()
			if err != nil {
				t.Fatal(err)
				conns <- i
			}
			conns <- 0
		}(i)
	}

	for i := 0; i < testCount; i++ {
		index := <-conns
		if index != 0 {
			println("login failed: ", index, "\n")
		}
	}

	status, err = getStatus()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("after: %s", status)

	for i := 0; i < testCount; i++ {
		clients[i].disconnect()
	}

	status, err = getStatus()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("end: %s", status)
}
