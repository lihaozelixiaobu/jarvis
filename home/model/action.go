//
// Create by Zeng Yun on 2018/12/22
//

package model

import "encoding/json"

// 动作
type Action struct {
	AId   string      `json:"aid" bson:"aid"`
	SId   int         `json:"sid" bson:"sid"`
	CId   int         `json:"cid" bson:"cid"`
	Value interface{} `json:"value" bson:"value"`
}

func (a *Action) String() string {
	buf, _ := json.Marshal(a)
	return string(buf)
}
