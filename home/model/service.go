//
// Create by Zeng Yun on 2018/12/22
//

package model

const (
	TypeAccessoryInformation = "1" // 配件基本属性
	TypeAccessoryBridge      = "2" // 桥接服务
	TypeAccessoryDFU         = "3" // 固件升级

	TypeContactSensor         = "11" // 接触传感器: 门磁
	TypeHTSensor              = "12" // 湿度计
	TypeLightSensor           = "13" // 光线传感器
	TypeOutlet                = "14" // 插座
	TypeSwitch                = "15" // 开关
	TypeAirConditioner        = "16" // 空调
	TypeClock                 = "17" // 闹钟
	TypeMotion                = "21" // 人体运动检测
	TypeColorLight            = "18" // 彩色灯
	TypeTemperatureColorLight = "19" // 色温灯
	TypeZigbeeCoordinator     = "20" // Zigbee协调器
)

type Service struct {
	AId  string `json:"aid" bson:"aid"`
	SId  int    `json:"sid" bson:"sid"`
	Name string `json:"name" bson:"name"`
	Type string `json:"type" bson:"type"`
	Icon string `json:"icon" bson:"icon"`
	Room string `json:"room" bson:"room"`

	// 状态改变是否需要推送消息
	Notify bool `json:"notify" bson:"notify"`
}
