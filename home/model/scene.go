//
// Create by Zeng Yun on 2018/12/22
//

package model

const (
	SceneTypeBase   = "base"   // 系统默认场景 离家 回家 晚安 早起
	SceneTypeCustom = "custom" // 用户自定义场景
)

// 场景
type Scene struct {
	Id      string    `json:"id" bson:"id"`
	Name    string    `json:"name" bson:"name"`
	Icon    string    `json:"icon" bson:"icon"`
	Type    string    `json:"type" bson:"type"`
	Actions []*Action `json:"actions" bson:"actions"`
}
