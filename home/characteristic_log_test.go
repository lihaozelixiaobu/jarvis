package home

import (
	"adai.design/jarvis/common/db"
	"github.com/globalsign/mgo/bson"
	"testing"
	"time"
)

// 查询一天的历史记录
func TestCharacteristicLogQuery(t *testing.T) {
	t.Log("let's go!")

	session, err := db.GetSession()
	if err != nil {
		t.Fatal(err)
	}
	defer session.Close()

	collection := session.DB("home").C(CharLoggerDBCollection)

	aid := "00158d0002379233"
	sid := 1
	cid := 1
	before := time.Now()
	after := before.Add(-time.Hour * 24)

	var list []*characteristicMarkCell
	err = collection.Find(
		bson.M{"aid": aid, "sid": sid, "cid": cid, "time": bson.M{"$gte": after, "$lte": before}}).Sort("-time").Limit(2).All(&list)

	t.Logf("len = %d", len(list))
	for _, cell := range list {
		date := cell.Time.In(time.Local).Format("2006-01-02 15:04:05")
		t.Logf("%s %v", date, cell.Value)
	}
}
