//
// Create by Zeng Yun on 2018/12/20
//

package home

const (
	pathHome                    = "home"
	pathHomeInfo                = "home/info"
	pathContainer               = "home/container"
	pathContainerCharacteristic = "home/container/characteristic"
	pathCharacteristic          = "home/characteristic"
	pathScene                   = "home/scene"
	pathCharacteristicLog       = "home/characteristic/log"

	pathMember         = "home/member"
	pathMemberLocation = "home/member/location"
)
